import lcm
import numpy as np
import matplotlib.pyplot as plt
import time
import math
import os
import getopt
import sys
import csv
sys.path.append('./lcmtypes')
from ctrlpanel import ctrlPanelData_t

f1 = []
f2 = []
t = []
plt.ion()
fig = plt.figure()
plt.title('Control Panel')

def plot_handler(channel, data):
    msg = ctrlPanelData_t.decode(data)
    f1.append(msg.fader1_scaled)
    f2.append(msg.fader2_scaled)
    t.append(msg.timestamp)
    fig.canvas.draw()

lc = lcm.LCM()
subscription = lc.subscribe("CTRL_PANEL_DATA", plot_handler)

current_path = os.getcwd()
filename = "impulse_response"

try:
    while True:
        lc.handle()
        plt.plot(t, f1, 'r')
        plt.plot(t, f2, 'b')

except KeyboardInterrupt:
    plt.savefig(os.path.join(current_path, filename+'.png'))
    myfile = open(filename+'.csv', 'wb')
    wr = csv.writer(myfile)
    wr.writerow(t)
    wr.writerow(f1)
    wr.writerow(f2) 

lc.unsubscribe(subscription)

