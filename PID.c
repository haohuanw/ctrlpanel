/*********************
 *   PID.c
 *   simple pid library 
 *   pgaskell
 **********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t * PID_Init(float Kp, float Ki, float Kd) {
    // allocate memory for PID
    PID_t *pid =  malloc(sizeof(PID_t));

    //pid params are [0,1]
    pid->pidSetpoint = 0.0001;
    pid->ITerm = 0.0;
    pid->derivative = 0.0;
    PID_SetTunings(pid, Kp, Ki, Kd);
    PID_SetIntegralLimits(pid, ITERM_MIN, ITERM_MAX);
    
    pid->kp = 0.012252;
    pid->ki = 0.015122;
    pid->kd = 0.000129;
    // set update rate to 100000 us (0.1s)
    PID_SetUpdateRate(pid, 100000);

    //set output limits, integral limits, tunings
    printf("initializing pid...\r\n");
    PID_SetOutputLimits(pid, MIN_OUTPUT, MAX_OUTPUT);
    return pid;
}

void PID_Compute(PID_t* pid) {    
    // Compute PID 
    float updateRateInSec = ((float) pid->updateRate / 1000000.0);
    pid->derivative = (pid->pidInput - pid->prevInput)/updateRateInSec;
    if(fabsf(pid->pidInput) > pid->pidSetpoint){
        pid->ITerm += pid->pidInput*updateRateInSec;
    }
    if(pid->ITerm > pid->ITermMax){
        pid->ITerm = pid->ITermMax;
    }
    else if(pid->ITerm < pid->ITermMin){
        pid->ITerm = pid->ITermMin;
    }
    pid->prevInput = pid->pidInput;
    pid->pidOutput = (1000000/(float)pid->updateRate)*(pid->kp*pid->pidInput + pid->ki*pid->ITerm + pid->kd*pid->derivative);
    /*if(fabsf(pid->pidInput) < pid->pidSetpoint){
        pid->pidOutput = 0.0;
    }*/
    if(pid->pidOutput > pid->outputMax){
        pid->pidOutput = pid->outputMax;
    }
    else if(pid->pidOutput < pid->outputMin){
        pid->pidOutput = pid->outputMin;
    }
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
    //scale gains by update rate in seconds for proper units
    float updateRateInSec = ((float) pid->updateRate / 1000000.0);
    //set gains in PID struct
    //TODO: Not sure of doing this
    pid->ki = 10*Ki*updateRateInSec;
    pid->kp = 10*Kp*updateRateInSec;
    pid->kd = 0.05*Kd*updateRateInSec;
}

//TODO: What's the need for these 3 functions
void PID_SetOutputLimits(PID_t* pid, float min, float max){
    //set output limits in PID struct
    pid->outputMin = min;
    pid->outputMax = max;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
    //set integral limits in PID struct
    pid->ITermMin = min;
    pid->ITermMax = max;
}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
    //set update rate in PID struct
    pid->updateRate = updateRate;
}




